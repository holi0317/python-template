# python-template

Template for a new python project.

## Features

We are using following tools in the project

- poetry for dependency management
- black and black for code formatting
- flake8 for linting
- Editorconfig included
- Gitignore file included
- Gitlab CI for running CI jobs
- pre-commit configured
- Project-wide configuration for vscode and coc (vim)

## Usage

1. Clone this repository
2. Add a new project on gitlab
3. Set origin url for the cloned repository to the new project
4. Do a global search on `python-template` and replace them with new project name
5. Rewrite this README
6. Create a directory with the new project name and put python code there
